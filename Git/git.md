
# Using git to keep track of your project

## Summary
1. [Motivation](#markdown-header-1-motivation)
    - Version control automatically keeps a record of your project
    - Complete history of project
    - Ability to revert changes
    - Collaboration is easy
    - Entire history can be on multiple machines
1. [Basic usage](#markdown-header-2-basic-usage)
    - [Initializing a repository](#markdown-header-initializing-a-repository)
    - [What not to git](#markdown-header-what-not-to-git)
    - [Staging and committing changes](#markdown-header-staging-and-committing-changes)
    - [Ignoring files](#markdown-header-ignoring-files)
    - [Undoing changes](#markdown-header-undoing-changes)
1. Remote repositories
    - Advantages
    - How it works
    - Getting started
    - `git pull`
    - `git push`
    - `git clone`
1. Branches
    - Why use branches
    - `git branch`
    - `git merge`
    - `git rebase`

## 1. Motivation

![Final.doc](images/phd_comics_phd101212s.gif)

`git` is a **version control** program which makes it easy to keep a complete history of your project. After you set it up, it will maintain a log of the edits you make to documents. Conveniently, the history is stored separately from the document so you don't need to create a new file every time you want to work on your project. You can also review and undo changes to individual files in your project, allowing you to "mix and match" from your project's history. If you have multiple computers, git makes it easy to sync your project and keep all your machines up to date.

![On the other hand, me-from-6-months-ago responds promptly](images/git_motivation_tweet.png)

Version control systems, and `git` in particular, are designed for easy collaboration. Your project's history includes the author of each change. If multiple people are working on a file, git makes it easy to merge the two versions of the document and check for conflicts. You can also upload your project to a remote server (e.g., GitHub or BitBucket) which lets anyone comment on and suggest changes to your project. 

## 2. Basic usage

Git is fairly simple to use. Most commands are of the form

    git <command> <options>

Additionally, it is nearly impossible to irreparably mess up your history.

Below, I'll go through the basics of using git and provide examples of how to use git to manage your project.

### Initializing a repository
In order to use git you must first initialize a repository, which is the term for a folder tracked by git. Once you initialize a repository in a folder, all files and subfolders in your repository are included. 

**Warning: Do not initialize a repository within an existing repository. This can make the size of the outer-most repository explode**

In a bash shell (Git Bash on Windows, terminal on Mac and Linux), navigate to the folder you wish to track by typing

    cd <path to folder>
    
Then type the command

    git init
    
Your repository has now been initialized. You can check this by typing

    git status
    
You should see something like

    On branch master
    
    Initial commit
    
    nothing to commit (create/copy files and use "git add" to track)
    
You can use `git status` at any time to check the status of your repository. It is very useful to keep track of which changes have been tracked and which haven't.

### What not to git
It is generally inadvisable to track the following types of data with git:
- Large binary files
    - **Example**: Anything that is not text, such as word documents, pdfs, or applications
    - **Reason**: git was designed to work efficiently with text, tracking changes to binary files can take up a lot of disk space
    - *Note*: This is not a hard and fast rule. It can be useful to include images in your repository for example
- Secret data
    - **Example**: Passwords, sensitive data, anything you'd like to keep private
    - **Reason**: Anyone you collaborate with can see your entire repository. [This problem is so common that GitHub has a page devoted to fixing it](https://help.github.com/articles/removing-sensitive-data-from-a-repository/).
- Machine specific settings
    - **Example**: Configuration files
    - **Reason**: Configuration will vary by machine, so it doesn't make sense to sync them across machines

### Staging and committing changes
One of the most useful features of git is that you have very fine control over what files are tracked. The trade off to this is that by default, nothing is tracked. You have to tell git exactly what you want tracked.

Tracking in git is a two step process. First you "stage" your changes then you "commit" them to the repository. After the changes have been committed they are part of the (mostly) permanent part of your project's history. The two commands to do this are `git add` and `git commit`. The `git status` command will be your best friend when staging and committing files since it tells you exactly which files have been changed and which files have been staged.

![](images/git-add-commit.png)
([newtFire, Explain Git Shell](http://newtfire.org/dh/explainGitShell.html))

#### Staging changes
After making changes to a file, you need to stage them so that git knows to track the changes. You can check if you have unstaged commits by typing `git status`:

    On branch master
    Changes not staged for commit:
      (use "git add <file>..." to include in what will be committed)
      (use "git checkout -- <file>..." to discard changes in working directory)
      
           modified: modified_file.txt
           
    no changes added to commit (use "git add" and/or "git commit -a")
    
Here, git is telling me that I have not staged anything, but one of my files has been modified.

When I'm ready to track the changes in this file, I stage them by typing `git add modified_file.txt`. `git status` shows:

    On branch master
    Changes to be committed:
      (use "git reset HEAD <file>..." to unstage)
      
            modified: modified_file.txt
            
As indicated in the status message, we can unstage the changes by typing `git reset HEAD <file>`, which tells git "undo all the changes in my file since the last commit".

#### Committing changes
When you're satisfied with your changes and ready to make them part of your project's history, you're ready to commit them. 

First, stage your changes as above. Then type `git commit -m "Commit message"`. The commit message is a brief comment explaining your changes. It will show up in your history and makes it easier to find specific changes you made in the past.

After running this command, typing `git status` shows us that we have no changes:

    On branch master
    nothing to commit, working directory clean
    
We can verify that our changes were committed by looking at the log with `git log`:

    commit 82a7f87b1de59ec8739f0547191f23011428303c
    Author: Taylor D. Scott <tscott5@wisc.edu>
    Date:   Thu Jul 6 10:47:03 2017 -0500

        made some more changes

    commit 104835e8e127a7e234ccb2fb1bbda117ad3086d6
    Author: Taylor D. Scott <tscott5@wisc.edu>
    Date:   Thu Jul 6 10:45:43 2017 -0500

        made some changes
        
    <...>

The git log shows a few key pieces of information for each commit: the *hash* (a unique identifier), author, date, and commit message. As your projects grow, your git log will get longer and it can be convenient to display an abbreviated log with `git log --oneline` which shows the first 7 characters of the hash and the commit message:

    82a7f87 made some more changes
    104835e made some changes
    c1058c2 wrote some text
    2a55892 created a file
    0ab1daa first commit

You chan also show only the last `k` commits with `git log -n <k>`. For exmaple, `git log -n 2 --oneline` gives:

    82a7f87 made some more changes
    104835e made some changes

### Ignoring files
There are some files you *don't* want git to track. These may be, for example, machine specific configuration files, files containing passwords, or large binary (non-text) files. One way to ignore these files is simply not ever staging them for commit. But this can clutter up your git status, for example:

    On branch master
    Changes not staged for commit:
      (use "git add <file>..." to update what will be committed)
      (use "git checkout -- <file>..." to discard changes in working directory)

            modified:   modified_file.txt

    Untracked files:
      (use "git add <file>..." to include in what will be committed)

            large_file.pdf
            sensitive.txt
            settings.config

    no changes added to commit (use "git add" and/or "git commit -a")
    
You can tell git to ignore the files. Then git will pretend that they don't exist. You can do this by creating a file called `.gitignore` (be sure to include the dot). In this file, list the files you want git to ignore, each on its own line. So in the example above, the `.gitignore` file might look like

    large_file.pdf
    sensitive.txt
    settings.config
    
Now runnings `git status` shows

    On branch master
    Changes not staged for commit:
      (use "git add <file>..." to update what will be committed)
      (use "git checkout -- <file>..." to discard changes in working directory)

            modified:   modified_file.txt

    Untracked files:
      (use "git add <file>..." to include in what will be committed)

            .gitignore

    no changes added to commit (use "git add" and/or "git commit -a")
    
It's good to commit the `.gitignore` file to your repository so that the same files are ignored on every machine.

You can ignore entire directories just as you would files, and if you have a lot of files to ignore you can use wildcards. For example:

    *.pdf
    secret_directory
    config/user-*.config

Now git will ignore pdfs, everything in `secret directory` and all `.config` files in the folder `config` which start with `user-`.

### Undoing changes

![This is fine](images/this-is-fine.png)

While working on your project, you will inevitably realize you've made a mistake and need to undo your changes. Fortunately, git saves your entire working history so you know exactly what your project looked like the last time it worked.

Git is a powerful tool for undoing changes. Unfortunately, with great power comes great inconvenience. You may need to use a different command depending on *where* you are in the edit-stage-commit process. Luckily, it is nearly impossible to completely destroy your history; only one command `git reset` will erase committed changes. The other commands in this section keep your history around so you can recover if you make a mistake.

#### Undo changes that haven't been staged
This is the easiest case. You've made changes to a file, but you haven't staged or committed them. `git checkout HEAD -- <file>` will delete these changes and restore your working directory to your last commit. 

#### Undo staged changes
This is a two step process. First, unstage the file with `git reset HEAD -- <file>` then restore your working directory with `git checkout HEAD -- <file>` as above.

#### Undo committed changes
Here's where it gets more complicated. There are two things to keep in mind when you need to undo a commit. The first is whether you need to undo an entire commit, or just changes to one file. The second is whether you've already shared this commit or if it only exists on your local machine. We'll go through undoing entire commits first, since it's conceptually simpler. 

##### Undo a commit that only exists on your local machine
Use caution with this, it will erase your history. It should only be used if you absolutely cannot keep a record of the bad commit. For example, this is the command to use if you actually commit your password to the repository. Remember, you should only use this if you HAVE NOT shared your commit with anybody else. 

`git reset --hard <last good commit>` where `<last good commit>` is the hash of the last commit you want to keep. Your working directory will be changed to match `<last good commit>`. **THIS IS PERMANENT.**

##### Undo a shared commit
If you've shared your commit with someone else, it's a bad idea to rewrite history. You might break whatever they're working on. Instead, the safe way to handle this is to create a new commit which undoes the mistakes. So, the mistakes will still exist in the commit history, but they will be gone in the most recent commit.

The command to do this is `git revert <bad commit>`. When you run this command, you will be taken to a text editor which allows you to change the commit message. Generally the default is fine, but you can add more details if you wish.

Importantly, this only undoes the changes in a single commit. If you need to undo multiple commits, you can run the command `git revert <bad commit 1> <bad commit 2> <and so on> --no-commit`. You can include as many commits as you want. This will undo the changes in the bad commits and add them to your staging area. You can then commit these changes as usual. **This is the safest way to undo commits. Use this unless you are absolutely sure you need to use git reset.**

##### Undo changes to a single file
There's (surprisingly) only one option here: `git checkout <good commit> -- <file>` where `<good commit>` is the hash of the commit you wish to restore. This will change your working version of `<file>` to match that in `<good commit>` and add it to the staging area. Then you commit the change as you normally would. There is not an easy or safe way to erase bad changes to a particular file from your project's history. 

## 3. Remote repositories

## 4. Branches
