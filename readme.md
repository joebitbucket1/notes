# Notes

Collection of notes I'm writing about various things (mostly software). These are intented to serve as a reference and cheat sheet for myself and others.

The links below will take you to the most recent version of each set of notes. You can view previous versions by looking at the source.

Feel free to fork and make changes!

## [Git](Git/git.md)